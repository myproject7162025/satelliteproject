package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@Service
public class TableCreationService {
	@Autowired
	private EntityManager entityManager;

	@Transactional
	public StringBuilder createTable(String tableName, List<String> columns) {
//		StringBuilder sql = new StringBuilder("CREATE TABLE IF NOT EXISTS ");
		StringBuilder sql = new StringBuilder("CREATE TABLE ");
		sql.append(tableName).append(" (");

		for (int i = 0; i < columns.size(); i++) {
			String column = columns.get(i);
			sql.append(column);
			if (i < columns.size() - 1) {
				sql.append(" varchar2(100), ");
			}
			if(i==columns.size()-1) {
				sql.append(" varchar2(100)");
			}
		}
		sql.append(");");

		jakarta.persistence.Query query = entityManager.createNativeQuery(sql.toString());
		query.executeUpdate();
		return sql;
	}

//	@Transactional
//	public void insertData(String tableName, Map<String, Object> data) {
//		StringBuilder sql = new StringBuilder("INSERT INTO ");
//		sql.append(tableName).append(" (");
//
//		data.keySet().forEach(key -> sql.append(key).append(", "));
//		sql.setLength(sql.length() - 2); // Remove last comma and space
//		sql.append(") VALUES (");
//
//		data.values().forEach(value -> sql.append("'").append(value).append("', "));
//		sql.setLength(sql.length() - 2); // Remove last comma and space
//		sql.append(")");
//
//		Query query = entityManager.createNativeQuery(sql.toString());
//		query.executeUpdate();
//	}
}
