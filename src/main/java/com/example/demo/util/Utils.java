package com.example.demo.util;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Utils {
	public static List<String> extractor(String s) {
		String replace = s.replace(",", "");
		String[] split = replace.split(" ");
		int fieldsIndex = -1;
		for (int i = 0; i < split.length; i++) {
			if (split[i].equals("fields")) {
				fieldsIndex = i;
				break;
			}
		}
		// Extract words after "fields"
		String[] output = new String[0];
		if (fieldsIndex != -1 && fieldsIndex < split.length - 1) {
			output = Arrays.copyOfRange(split, fieldsIndex + 1, split.length);
		}
		return Arrays.asList(output);
	}

	public static List<String> getColumns() throws SQLException {
		List<String> list = new ArrayList<>();
		Connection connection = DriverManager.getConnection("jdbc:h2:mem:project", "sa", "");
		DatabaseMetaData metaData = connection.getMetaData();
		ResultSet resultSet = metaData.getColumns(null, null, "DYNAMIC_TABLE", null);
		while (resultSet.next()) {
			String columnName = resultSet.getString("COLUMN_NAME");
			list.add(columnName);
		}
		resultSet.close();
		connection.close();
		return list;
	}
}
