package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Lokesh Reddy
 */
@SpringBootApplication
public class SearchEngineCrudGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SearchEngineCrudGeneratorApplication.class, args);
	}

}
