package com.example.demo.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.TableCreationService;
import com.example.demo.util.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class SearchController {

	private final TableCreationService tableService;

	@PostMapping("/create-table")
	public String PostMethodName(@RequestBody Map<String, String> input) {
		List<String> extractor = Utils.extractor(input.get("input"));
		tableService.createTable("dynamic_table", extractor);
		return "dynamic table created";
	}

	@GetMapping("/hi")
	public String greeting() {
		return "Hi Lokesh";
	}

	@GetMapping("/get-columns")
	public List<String> getColumns() throws SQLException {
		return Utils.getColumns();
	}

	@PostMapping("/insert")
	public String insertData(@RequestBody String input) throws SQLException, JsonProcessingException {
		List<String> columns = Utils.getColumns();
		ObjectMapper obj = new ObjectMapper();
		Map<String, Object> value = obj.readValue(input, new TypeReference<Map<String, Object>>() {
		});
		System.out.println(value);
//		return dynamicService.saveData(pojo);
		return "value";
	};

}
